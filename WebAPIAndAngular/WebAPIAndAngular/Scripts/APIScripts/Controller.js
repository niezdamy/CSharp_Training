﻿app.controller('APIController', function ($scope, APIService) {

    getAll();

    function getAll() {
        var servCall = APIService.getStudents();
        servCall.then(function (result) {
            $scope.students = result;
        }, function (error) {
            console.log('Oops! Something went wrong while fetching the data.')
        });
    }

    $scope.saveStudent = function () {
        var student = {
            FirstName: $scope.FirstName,
            LastName: $scope.LastName,
            Rate: $scope.Rate
        };
        var saveSubs = APIService.saveStudent(student);
        saveSubs.then(function(d) {
                getAll();
            },
            function(error) {
                console.log('Oops! Something went wrong while saving the data.')
            });
    };
   
    $scope.dltStudent = function (id) {
        var dlt = APIService.deleteStudent(id);
        dlt.then(function(d) {
                getAll();
            },
            function(error) {
                console.log('Oops! Something went wrong while deleting the data.')
            });
    };
});
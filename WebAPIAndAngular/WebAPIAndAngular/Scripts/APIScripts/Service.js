﻿app.service("APIService", function ($http) {
    this.getStudents = function () {
        var url = 'api/Student';
        return $http.get(url).then(function (response) {
            return response.data;
        });        
    }

    this.saveStudent = function (student) {
        return $http({
            method: 'post',
            data: student,
            url: 'api/Student'
        });
    }
    
    this.deleteStudent = function (studentId) {
        var url = 'api/Student/' + studentId;
        return $http({
            method: 'delete',          
            url: url
        });
    }
});